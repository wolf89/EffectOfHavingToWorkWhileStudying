---
title: "Auswertung"
author: "Maximilian Göhner, Christoph Graebnitz, Dominik Bechinie"
date: "June 19, 2017"
output:
  html_document:
    toc: true
    toc_float: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(warning = FALSE)
knitr::opts_chunk$set(message = FALSE)
```

```{r library inread, include=FALSE}
library(data.table)
library(likert)
library(psych)
library(pspearman)
library(Hmisc)
library(ggplot2)
library(waffle)
library(ggthemes)
library(scales)
#library(corrgram)
#library(reshape) bummst Likert (hahaha, wasn scheiss)
library(corrplot)
```

```{r, include=FALSE}
setwd("/home/maximilian/Dropbox/Uni/Master/4. Fachsemester/Empirische Bewertung Informatik/Auswertung/Empirie")
#setwd("/home/christoph/Dropbox/UniGitRepos/Empirische Bewertung in der Informatik/Empirie")
```


# Einlesen und Formatieren der Rohdaten
```{r Dateninput}
daten = read.table("EmpirieCSVStudiumArbeit.csv",
                   header = TRUE,
                   sep = ",",
                   strip.white=TRUE,
                   na.strings=c("NA", "-", "?","keine Angabe"))

fragen = read.table("questions.txt",
                    header = TRUE,
                    sep=";")

df_demographic = daten[,c("Geschlecht",
                          "Alter",
                          "Hochschule",
                          "Abschluss",
                          "Hauptfach",
                          "SemesterJetzt",
                          "SemesterVorraussichtlich",
                          "Finanzierung")]

#Unsere Likert Skala
skala = c("Ich stimme nicht zu",
          "Ich stimme eher nicht zu",
          "Ich stimme eher zu",
          "Ich stimme zu") 
          #,"keine Angabe")

#Funktion, die die Level ändert
changeLevels <- function(x){
  if(is.factor(x)) return(factor(x, levels=skala))
  return(x)
}

#Änderung aller Levels der Likert-Items
daten[,c(2:17,19:29,31:56,58:62)] = as.data.frame(lapply(daten[,c(2:17,19:29,31:56,58:62)], changeLevels))

#Ersetzung der Überschriften durch komplette Frage
colnames(daten)[c(2:17,19:29,31:56,58:62)] = as.character(fragen[c(2:17,19:29,31:56,58:62),1])
```


# Uebung 10
## 1a Access
Folgende Links sind im Projekt-Wiki hinterlegt, die zu diesem Report und unseren Daten als .csv führen:

* https://www.dropbox.com/s/7v2m661aj0eysk2/Auswertung.html?dl=0
* https://www.dropbox.com/s/r7amqjyurjrtzea/EmpirieCSVStudiumArbeit.csv?dl=0

## 1b Validity of Data
Unsere Daten sind bis auf folgende Kleinigkeiten valide:

* Zwei Personen müssen die Frage zum Studienende falsch verstanden haben. Denkbar ist, dass diese anstatt das Semester anzugeben, in dem sie vorraussichtlich ihr Studium abschließen, die Anzahl an Semestern angegeben haben, die sie noch benötigen. -> Vorgehen wird besprochen
* 2 Personen haben bei Geschlecht "Alien" angegeben, ein Guppenmitglied aber dann auf ihren Scherz angesprochen -> werden als männlich identifiziert (Geschlecht ist bei uns ja auch nichts vorrangig wichtiges)
* Hochschulen wurden orthografisch sehr unterschiedliche beschrieben -> werden von uns zusammengefasst, siehe unten
* Studienfächer ebenso


## 2a Graphical Overview of the Demographic Profile

### Wafflecharts

Geschlecht
```{r Geschlecht, warning=FALSE}
gender <- table(df_demographic$Geschlecht)
gender <- round(gender/sum(gender)*100)
waffle(gender[c(2,1,3)]) +
  scale_fill_tableau(name=NULL) +
  labs(title="Geschlecht", subtitle="in %")
```

Arbeitsstatus
```{r Arbeit}
job <- table(daten$X03_01_arbeit)
job <- round(job/sum(job)*100)
waffle(job) +
  scale_fill_tableau(name=NULL) +
  labs(title="Arbeit Ja/Nein", subtitle="in %")
```


### Histogramme
#### Hochschule
```{r Hochschule}
df_demographic$Hochschule <- toupper(df_demographic$Hochschule)

# join college names
beuth <- df_demographic$Hochschule %in% c('BEUTH',
                                          'BEUTH HOCHSCHULE',
                                          'BEUTH HOCHSCHULE FÜR TECHNIK BERLIN',
                                          'BEUTH HOCHSCHULE')



humboldt <- df_demographic$Hochschule %in% c('HUMBOLDT-UNIVERSITÄT BERLIN',
                                             'HU UND TU')

wildau <- df_demographic$Hochschule %in% c('TH WILDAU')

df_demographic$Hochschule[beuth] <-'BEUTH HOCHSCHULE'
df_demographic$Hochschule[humboldt] <- 'HUMBOLDT-UNIVERSITÄT BERLIN'
df_demographic$Hochschule[wildau] <- 'TH WILDAU'

#build table from college names for histogram
t <- data.frame(table(df_demographic$Hochschule))

#plot barplot
ggplot(t, aes(x=reorder(Var1, -Freq), y=Freq)) +
  geom_bar(stat="identity", fill = "lightblue") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  xlab("Hochschule") +
  ylab("Häufigkeit") +
  ggtitle("Verteilung der Teilnehmer auf Hochschulen")
```

#### Hauptfach
```{r Hauptfach}
#table for hist
t <- data.frame(table(df_demographic$Hauptfach))
#plot
ggplot(t, aes(x=reorder(Var1, -Freq), y=Freq)) +
  geom_bar(stat="identity", fill = "lightblue") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  xlab("Hautpfach") +
  ylab("Häufigkeit") +
  ggtitle("Verteilung auf Hauptfächer")
```

#### Abschluss
```{r Abschluss}
ggplot(df_demographic, aes(df_demographic$Abschluss)) +
  geom_bar(fill = "lightblue") +
  xlab("angestrebter Abschluss") + 
  ylab("Häufigkeit")
```

#### Alter
```{r Alter}
ggplot(data=df_demographic, aes(df_demographic$Alter)) +
  geom_histogram(fill = "lightblue") +
  scale_x_continuous(breaks=seq(15,40,1)) +
  xlab("Alter der TeilenehmerInnen") + 
  ylab("Häufigkeit")
```

```{r Shapiro}
shapiro.test(daten$Alter)
```
-> nicht normalverteilt

#### Semester
```{r Semester}
semesterToGo = df_demographic$SemesterVorraussichtlich-df_demographic$SemesterJetzt #hier zwei Ausreißer im negativen Bereich!
ggplot() + 
  geom_histogram(aes(x = semesterToGo), fill="lightblue") +
  scale_x_continuous(breaks=seq(-8,8,1)) +
  xlab("Anzahl vorraussichtlich noch zu absolvierender Semester") + 
  ylab("Häufigkeit")
```

### Distribution of answers

```{r jitterPlot}
jittered = daten[,c(1:17,19:29,45:56,31:37,38:44,58:62)]
jitteredmelt = melt(jittered,id.vars = "Zeitstempel", measure.vars = colnames(jittered)[2:56])
ggplot(jitteredmelt, aes(x = variable, y = value, group=Zeitstempel)) +
  geom_point(size =0.5, position = position_jitter(w = 0.2, h = 0.2)) +
  #geom_line(position = position_jitter(w = 0.05, h = 0.05)) + #doesnt work
  #coord_fixed(ratio = 2.0) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 7)) +
  theme(legend.position="none")
```

```{r Likertskalen gesamt,collapse=TRUE, fig.show="hold"}
#producing likert scales
studiumLikert = likert(daten[,2:9])
studienerfolgLikert = likert(daten[,10:17])
motivationMitLikert = likert(daten[,19:29])
motivationOhneLikert = likert(daten[,45:56])
naeheArbeitMitLikert = likert(daten[,31:37])
naeheArbeitOhneLikert = likert(daten[,38:44])
arbeitStudiumLikert = likert(daten[,58:62])

#par(mfrow=c(1,2))
layout(matrix(c(1,2,3,4,5,6,7,7), 4, 2, byrow = TRUE))

#plotting them
plot(studiumLikert,
     ordered = FALSE)
plot(studienerfolgLikert,
     ordered = FALSE)
plot(motivationMitLikert,
     ordered = FALSE)
plot(motivationOhneLikert,
     ordered = FALSE)
plot(naeheArbeitMitLikert,
     ordered = FALSE)
plot(naeheArbeitOhneLikert,
     ordered = FALSE)
plot(arbeitStudiumLikert,
     ordered = FALSE)
```

```{r motivation_mit_vs_ohne}
#daten so formatieren, dass nur mtivationsitems betrachtet werden und beide mit keywords benannt sind
#motivationMitLikert = likert(daten[,19:29])
#motivationOhneLikert = likert(daten[,45:56])

motivationmitarbeit = daten[,19:29]
motivationohnearbeit = daten[,c(45:54,56)]
headlines = c("FinanziellAngewiesen",
              "Spass",
              "Sonstlangweilig",
              "Beruf",
              "Studium",
              "neueLeute",
              "-finanziellUnabhängig",
              "-Noten",
              "-Stress",
              "-keinen Spass",
              "-studienfern")

colnames(motivationmitarbeit) = headlines
colnames(motivationohnearbeit) = headlines

motivationmitarbeit$Arbeit = c(TRUE)
motivationohnearbeit$Arbeit = c(FALSE)

full = rbind(motivationmitarbeit,motivationohnearbeit)
fullmolten = melt(data = full, id.vars = "Arbeit", na.rm = T)
fullmolten$value = factor(fullmolten$value, levels = c("Ich stimme nicht zu",
                                                       "Ich stimme eher nicht zu",
                                                       "Ich stimme eher zu",
                                                       "Ich stimme zu"),ordered = TRUE)
fullmolten$variableArbeit = interaction(fullmolten$variable, fullmolten$Arbeit)

#add two scales top/bottom and plot both dataspaces in same plot
ggplot(fullmolten, aes(x = variable, y = value, group=variableArbeit, color=Arbeit)) +
  #geom_boxplot() +
  stat_summary(fun.y="median", geom="point") +
  #stat_summary(fun.y="mean", geom="point") +
  xlab("Frage") +
  ylab("Zustimmung") +
  coord_fixed(ratio = 2.0) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1, size = 10))
```

```{r correlograms}
corrdata = sapply(daten[,2:17], as.numeric)
colnames(corrdata) = c("SB_Regelstudienzeit",
                       "SB_Spass",
                       "SB_Neues",
                       "SB_Noten",
                       "SB_Schneller",
                       "SB_Leichtfallen",
                       "SB_Gehalt",
                       "Erfolgreich",
                       "ED_Regelstudienzeit",
                       "ED_Spass",
                       "ED_Neues",
                       "ED_Noten",
                       "ED_Schneller",
                       "ED_Leichtfallen",
                       "ED_Gehalt",
                       "Abgebrochen")
datencor = cor(as.matrix(corrdata), method="spearman", use="pairwise.complete.obs") #funktioniert mit ints
corrplot(datencor, method = "circle")
#rcorr(as.matrix(corrdata), type = "spearman")
```

```{r scatterplot eigener Studienerfolg mit Studienerfolg und Arbeit}
#negative Items repolarisieren
scatter = daten[,c(31:33,37:41,44,58:62)]
scatter = sapply(scatter, as.numeric)
scatter=scatter-1
colnames(scatter) = c("Anwendung",
                      "Pause",
                      "Nutzen",
                      "Ferne",
                      "Anwendung2",
                      "Pause2",
                      "Nutzen2",
                      "Naehe",
                      "Ferne2",
                      "Kein Einfluss",
                      "Fordern",
                      "Hindern",
                      "Deckung",
                      "Abwechslung")
scatter = as.data.frame(scatter)
scatter$Score = rowSums(scatter[,c(1,3,5,7,8,11,13)],na.rm = TRUE)-rowSums(scatter[,c(2,4,6,9,12,14)],na.rm = TRUE)

plot(y=scatter$Score,x=jitter(as.numeric(daten$`Ich schätze mein Studium als erfolgreich ein.`)),ylab ="Score",xlab = "Schätzung des Studiums als Erfolgreich")
#abline(lm(as.numeric(daten$`Ich schätze mein Studium als erfolgreich ein.`)~scatter$Score), col="red")
```

```{r Tabplot}
t = daten[,c(9,34)]
levels(t) = skala
t[,1] = factor(t[,1], ordered = TRUE)
t[,2] = factor(t[,2], ordered = TRUE)
tdata = table(t)
plot(tdata, las =2, main="Mosaikplot")
```


## 2b Thoughts about our Approach

### Interesting Data
* Vergleich der Vorstellungen zwischen Job/kein Job
* Kohärenz der Antworten zwischen Motivation zu arbeiten/aufzuhören
* Widerspruch "Arbeit sollte studiennah sein" und "Arbeit sollte Pause von Uni sein" individuell beleuchten


### Important Relationships
* Nähe der Arbeit zum Studium -> Studienerfolg + Einfluss Arbeit Studium
* Angaben zum eigenen Studium mit Angaben zum Studienerfolg
* Studiendauer/vorraussichtliche Studiendauer und Definition von Erfolg (eventuell Abgleich mit FU Daten zur Regelstudienzeit und durchschnittlich benötigten Zeit)


### Grouping Participants
* Arbeitend/Nichtarbeitend
* Nach grober Fachrichtung
* in Regelstudienzeit?
* Geschlecht?


## 2c Visualizing our Data
* Likertskalen mit dem likert() package
* gewichtete Scatterplots
* Lineplots, die das Antwortverhalten einzelner zeigen
* Correlogramme zwischen den Antworten

# TODOS
* kurzen Text zu den einzelnen demografischen Daten ergänzen
* Likerplots layouten